#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <ctype.h>

typedef enum
{
    STRIP_PUSHES,
    EXTRACT_PUSHES,
    STRIP_PUSH_DATA
} extract_mode;

size_t ExtractBytecodePattern(char *dst, const char *src, size_t srcLen, extract_mode parseMode);
size_t CountBytecodePushes(const char *src, size_t srcLen);
size_t GetBytecodePushedData(char *dst, const char *src, size_t srcLen, size_t pushIndex, int fromBehind);
size_t Bin2Hex(char *dst, const char *src, size_t srcStrlen);
size_t Hex2Bin(char *dst, const char *src, size_t srcLen);
void ReverseBytesHex(char *dst, const char *src, size_t srcStrlen);
void ReverseBytesBin(char *dst, const char *src, size_t srcLen);
uint64_t Bin2UIntLE(const char *src, size_t len);
size_t Int2BinScriptNum(char *dst, int64_t num);

// echoes byte stream unless it finds "scriptSig": "hex": "abcdef123"
// pattern in which case it parses it and injects scriptSig
int main(void)
{
    const char scriptSigToken[] = "\"scriptSig\":";
    const int scriptSigTokenLen = strlen(scriptSigToken);
    const char scriptPubKeyToken[] = "\"scriptPubKey\":";
    const int scriptPubKeyTokenLen = strlen(scriptPubKeyToken);
    const char hexToken[] = "\"hex\":";
    const int hexTokenLen = strlen(hexToken);
    char *scratchpad;
    char *inputBytecodeHex;
    char *remainder;
    char *inputBytecode;
    char *redeemBytecode;
    char *inputPattern;
    char *redeemPattern;
    char *inputPatternHex;
    char *redeemPatternHex;
    char *outputBytecodeHex;
    char *outputBytecode;
    char *outputPattern;
    char *outputPatternHex;
    scratchpad = malloc(16000000);
    inputBytecodeHex = scratchpad;
    remainder = &scratchpad[2000000];
    inputBytecode = &scratchpad[8000000];
    redeemBytecode = &scratchpad[9000000];
    inputPattern = &scratchpad[10000000];
    redeemPattern = &scratchpad[11000000];
    inputPatternHex = &scratchpad[12000000];
    redeemPatternHex = &scratchpad[14000000];
    outputBytecodeHex = inputBytecodeHex;
    outputBytecode = inputBytecode;
    outputPattern = inputPattern;
    outputPatternHex = inputPatternHex;

    int inScriptSig;
    int c = getchar();
    for(;;) {
        size_t i;
        // scan for inputs and inject pattern
        for(i = 0; c == scriptSigToken[i] &&
        i < scriptSigTokenLen && c != EOF; i++) {
            putchar(c);
            c = getchar();
        }
        if(i == scriptSigTokenLen) {
            // entered "scriptSig":
            for(;;) {
                // parse through scriptSig body
                for(i = 0; c == hexToken[i] &&
                i < hexTokenLen && c != EOF; i++) {
                    putchar(c);
                    c = getchar();
                }
                if(i == hexTokenLen) {
                    // entered "hex":
                    // eat whitespace
                    while(c != '\"') {
                        if(c == EOF) {
                            break;
                        }
                        putchar(c);
                        c = getchar();
                    }
                    // eat the beginning "
                    putchar(c);
                    c = getchar();
                    // reading raw hex
                    i = 0;
                    for(;;) {
                        if(c == EOF) {
                            inputBytecodeHex[i] = '\0';
                            printf("%s", inputBytecodeHex);
                            break;
                        }
                        if (c == '"') {
                            inputBytecodeHex[i] = '\0';
                            printf("%s", inputBytecodeHex);
                            putchar(c);
                            c = getchar();

                            size_t j = 0;
                            size_t k = 0;
                            // find input end while reading into buffer
                            for(;;) {
                                char sequenceToken[] = "\"sequence\":";
                                int sequenceTokenLen = strlen(sequenceToken);
                                for(k = 0; c == sequenceToken[k] &&
                                k < sequenceTokenLen && c != EOF; k++) {
                                    remainder[j] = c;
                                    j++;
                                    c = getchar();
                                }
                                if(k == sequenceTokenLen) {
                                    remainder[j] = '\0';
                                    int isP2sh = (strstr(remainder, "scripthash") != NULL);
                                    size_t inputBytecodeLen = Hex2Bin(inputBytecode, inputBytecodeHex, i);
                                    size_t inputPatternLen = ExtractBytecodePattern(inputPattern, inputBytecode, inputBytecodeLen, STRIP_PUSHES);
                                    size_t inputPatternHexLen = Bin2Hex(inputPatternHex, inputPattern, inputPatternLen);
                                    inputPatternHex[inputPatternHexLen] = '\0';
                                    printf(",\"inputPatternHex\":\"%s\"", inputPatternHex);
                                    if(isP2sh) {
                                        size_t redeemBytecodeLen = GetBytecodePushedData(redeemBytecode, inputBytecode, inputBytecodeLen, 0, 1);
                                        size_t redeemPatternLen = ExtractBytecodePattern(redeemPattern, redeemBytecode, redeemBytecodeLen, STRIP_PUSHES);
                                        size_t redeemPatternHexLen = Bin2Hex(redeemPatternHex, redeemPattern, redeemPatternLen);
                                        redeemPatternHex[redeemPatternHexLen] = '\0';
                                        printf(",\"redeemPatternHex\":\"%s\"", redeemPatternHex);
                                    }
                                    printf("%s", remainder);
                                    break;
                                }
                                if(c == EOF) {
                                    remainder[j] = '\0';
                                    printf("%s", remainder);
                                    break;
                                } else {
                                    remainder[j] = c;
                                    j++;
                                    c = getchar();
                                }
                            }
                            break;
                        } else {
                            inputBytecodeHex[i] = c;
                            i++;
                            c = getchar();
                        }
                    }
                }
                if(c == '}' || c == EOF) {
                    break;
                } else {
                    putchar(c);
                    c = getchar();
                }
            }
        }

        // scan for outputs and inject pattern
        for(; c == scriptPubKeyToken[i] &&
        i < scriptPubKeyTokenLen && c != EOF; i++) {
            putchar(c);
            c = getchar();
        }
        if(i == scriptPubKeyTokenLen) {
            // entered "scriptPubKey":
            for(;;) {
                // parse through scriptPubKey body
                for(i = 0; c == hexToken[i] &&
                i < hexTokenLen && c != EOF; i++) {
                    putchar(c);
                    c = getchar();
                }
                if(i == hexTokenLen) {
                    // entered "hex":
                    // eat whitespace
                    while(c != '\"') {
                        if(c == EOF) {
                            break;
                        }
                        putchar(c);
                        c = getchar();
                    }
                    // eat beginning "
                    putchar(c);
                    c = getchar();
                    // reading raw hex
                    i = 0;
                    for(;;) {
                        if(c == EOF) {
                            outputBytecodeHex[i] = '\0';
                            printf("%s", outputBytecodeHex);
                            break;
                        }
                        if (c == '"') {
                            outputBytecodeHex[i] = '\0';
                            printf("%s", outputBytecodeHex);
                            putchar(c);
                            c = getchar();
                            // inject output pattern
                            size_t outputBytecodeLen = Hex2Bin(outputBytecode, outputBytecodeHex, i);
                            size_t outputPatternLen = ExtractBytecodePattern(outputPattern, outputBytecode, outputBytecodeLen, STRIP_PUSHES);
                            size_t outputPatternHexLen = Bin2Hex(outputPatternHex, outputPattern, outputPatternLen);
                            outputPatternHex[outputPatternHexLen] = '\0';
                            printf(",\"outputPatternHex\":\"%s\"", outputPatternHex);
                            // inject protocolID
                            int isProtocolID = (strstr(outputBytecodeHex, "6a04") == &outputBytecodeHex[0]);
                            if(isProtocolID && strlen(outputBytecodeHex) >= 12) {
                                char protocolIDHex[9];
                                char protocolIDString[5];
                                memcpy(protocolIDHex, &outputBytecodeHex[4], 8);
                                protocolIDHex[8] = '\0';
                                printf(",\"protocolIDHex\":\"%s\"", protocolIDHex);
                                Hex2Bin(protocolIDString, protocolIDHex, 8);
                                protocolIDString[4] = '\0';
                                //TODO: sanitize this for json output
                                //printf(",\"protocolID\":\"%s\"", protocolIDString);
                            }
                            break;
                        } else {
                            outputBytecodeHex[i] = c;
                            i++;
                            c = getchar();
                        }
                    }
                }
                if(c == '}' || c == EOF) {
                    break;
                } else {
                    putchar(c);
                    c = getchar();
                }
            }
        }
        
        if(c == EOF) {
            free(scratchpad);
            return EOF;
        } else {
            putchar(c);
            c = getchar();
        }
    }
    free(scratchpad);
    return 0;
}

size_t Bin2Hex(char *dst, const char *src, size_t srcStrlen)
{
    size_t i = 0;
    for ( ; i < srcStrlen; i++) {
        sprintf(&dst[2*i], "%02x", (uint8_t)src[i]);
    }
    dst[2*i]='\0';
    return 2*i;
}

size_t Hex2Bin(char *dst, const char *src, size_t srcLen)
{
    uint8_t a;
    uint8_t b;
    size_t i;
    for (i = 0; i < srcLen; i+=2) {
        a = tolower(src[i]);
        b = tolower(src[i+1]);
        if (isxdigit(a) && isxdigit(b)) {
            if (isdigit(a)) {
                a = a - 0x30u;
            }
            if (islower(a)) {
                a = a + 0x0au - 0x61u;
            }
            if (isdigit(b)) {
                b = b - 0x30u;
            }
            if (islower(b)) {
                b = b + 0x0au - 0x61u;
            }
            dst[i/2] = (a << 4) | b;
        }
        else {
            break;
        }
    }
    return i/2;
}

void ReverseBytesHex(char *dst, const char *src, size_t srcStrlen)
{
    char bufa;
    char bufb;
    size_t i;
    for (i = 0; i < srcStrlen/2; i+=2) {
        bufa = src[i];
        bufb = src[i+1];
        dst[i] = src[srcStrlen - i - 2];
        dst[i+1] = src[srcStrlen - i - 1];
        dst[srcStrlen - i - 2] = bufa;
        dst[srcStrlen - i - 1] = bufb;
    }
    dst[srcStrlen]='\0';
}

void ReverseBytesBin(char *dst, const char *src, size_t srcLen)
{
    char buf;
    for (size_t i = 0; i < srcLen/2; i++) {
        buf = src[i];
        dst[i] = src[srcLen - i - 1];
        dst[srcLen - i - 1] = buf;
    }
}

uint64_t Bin2UIntLE(const char *src, size_t len)
{
    uint64_t number = 0ull;
    for (size_t i = 0; i < len; ++i) {
        number |= (uint64_t)(uint8_t) src[i] << i*8;
    }
    return number;
}

size_t Int2BinScriptNum(char *dst, int64_t num)
{
    size_t i = 0;
    if (num >= -1 && num <= 16) {
        dst[0] = (char)(0x50 + num);
    }
    else {
        int neg = (num < 0 ? 1 : 0);
        uint64_t unum = (neg ? -num : num);
        size_t hb = 7;
        while ((unum & (0xffull << hb*8)) == 0) {
            --hb;
        }
        dst[0] = hb+1;
        ++dst;
        for (i = 0; i <= hb; ++i) {
            dst[i] = (uint8_t)(unum >> i*8);
        }
        if (neg) {
            dst[i] |= (1u << 7);
        }
    }
    return i+1;
}

size_t ExtractBytecodePattern(char *dst, const char *src, size_t srcLen, extract_mode parseMode)
{
    size_t d = 0; // dst iterator
    size_t s = 0; // src iterator
    size_t cPushes = 0; // consecutive pushes
    uint8_t uc;
    while (s < srcLen) {
        uc = (uint8_t) src[s];
        if (uc <= 0x60u && uc != 0x50u) { // 0x50 is RESERVED, we treat it as executable, not as push op
            if (uc == 0x00u || uc >= 0x4fu) { // OP_N
                if (parseMode == STRIP_PUSHES) {
                    ++cPushes;
                }
                else if (parseMode == STRIP_PUSH_DATA) {
                    dst[d] = 0x51u;
                    ++d;
                }
                else if (parseMode == EXTRACT_PUSHES) {
                    dst[d] = uc;
                    ++d;
                }
                ++s;
            }
            else if (uc <= 0x4bu) { // OP_PUSHBYTES_N
                size_t len = uc;
                if (s + 1 + len > srcLen) {
                    fprintf(stderr, "warn: missing data s=%zu; uc=%02x\n", s, uc);
                    break;
                }
                if (parseMode == STRIP_PUSHES) {
                    ++cPushes;
                    ++s;
                    s += len;
                }
                else if (parseMode == STRIP_PUSH_DATA) {
                    d += Int2BinScriptNum(&dst[d], len);
                    ++s;
                    s += len;
                }
                else if (parseMode == EXTRACT_PUSHES) {
                    dst[d] = uc;
                    ++d;
                    ++s;
                    memcpy(&dst[d], &src[s], len);
                    s += len;
                    d += len;
                }
            }
            else if (uc == 0x4cu || uc == 0x4du || uc == 0x4eu) { // OP_PUSHDATA_N
                size_t sz = 1 << (uc - 0x4cu);
                if (s + 1 + sz > srcLen) {
                    fprintf(stderr, "warn: missing data s=%zu; uc=%02x\n", s, uc);
                    break;
                }
                size_t len;
                len = (size_t) Bin2UIntLE(&src[s + 1], sz);
                if (s + 1 + sz + len > srcLen) {
                    fprintf(stderr, "warn: missing data sz=%zu; len=%zu s=%zu; uc=%02x\n", sz, len, s, uc);
                    break;
                }
                if (parseMode == STRIP_PUSHES) {
                    ++cPushes;
                    ++s;
                    s += sz;
                    s += len;
                }
                else if (parseMode == STRIP_PUSH_DATA) {
                    d += Int2BinScriptNum(&dst[d], len);
                    ++s;
                    s += sz;
                    s += len;
                }
                else if (parseMode == EXTRACT_PUSHES) {
                    dst[d] = uc;
                    ++d;
                    ++s;
                    memcpy(&dst[d], &src[s], sz + len);
                    s += sz + len;
                    d += sz + len;
                }
            }
        }
        else {
            if (parseMode == STRIP_PUSHES) {
                if (cPushes > 0) {
                    d += Int2BinScriptNum(&dst[d], cPushes);
                    cPushes = 0;
                }
            }
            if (parseMode != EXTRACT_PUSHES) {
                dst[d] = uc;
                ++d;
                ++s;
            }
            else {
                ++s;
            }
        }
    }
    if (parseMode == STRIP_PUSHES && cPushes > 0) {
        d += Int2BinScriptNum(&dst[d], cPushes);
        cPushes = 0;
    }
    return d;
}

size_t CountBytecodePushes(const char *src, size_t srcLen)
{
    size_t s = 0; // src iterator
    size_t countPushes = 0;
    uint8_t uc;
    while (s < srcLen) {
        uc = (uint8_t) src[s];
        if (uc <= 0x60u && uc != 0x50u) { // 0x50 is RESERVED, we treat it as executable, not as push op
            if (uc == 0x00u || uc >= 0x4fu) { // OP_N
                ++countPushes;
                ++s;
            }
            else if (uc <= 0x4bu) { // OP_PUSHBYTES_N
                size_t len = uc;
                if (s + 1 + len > srcLen) {
                    fprintf(stderr, "warn: missing data s=%zu; uc=%02x\n", s, uc);
                    break;
                }
                ++countPushes;
                ++s;
                s += len;
            }
            else if (uc == 0x4cu || uc == 0x4du || uc == 0x4eu) { // OP_PUSHDATA_N
                size_t sz = 1 << (uc - 0x4cu);
                if (s + 1 + sz > srcLen) {
                    fprintf(stderr, "warn: missing data s=%zu; uc=%02x\n", s, uc);
                    break;
                }
                size_t len;
                len = (size_t) Bin2UIntLE(&src[s + 1], sz);
                if (s + 1 + sz + len > srcLen) {
                    fprintf(stderr, "warn: missing data sz=%zu; len=%zu s=%zu; uc=%02x\n", sz, len, s, uc);
                    break;
                }
                ++countPushes;
                ++s;
                s += sz;
                s += len;
            }
        }
        else {
            ++s;
        }
    }
    return countPushes;
}

size_t GetBytecodePushedData(char *dst, const char *src, size_t srcLen, size_t pushIndex, int fromBehind)
{
    if (fromBehind == 1) {
        pushIndex = CountBytecodePushes(src, srcLen) - 1 - pushIndex;
    }
    size_t d = 0; // dst iterator
    size_t s = 0; // src iterator
    size_t countPushes = 0;
    uint8_t uc;
    while (s < srcLen) {
        uc = (uint8_t) src[s];
        if (uc <= 0x60u && uc != 0x50u) { // 0x50 is RESERVED, we treat it as executable, not as push op
            if (uc == 0x00u || uc >= 0x4fu) { // OP_N
                if (pushIndex == countPushes) {
                    dst[d] = uc;
                    ++d;
                    break;
                }
                ++countPushes;
                ++s;
            }
            else if (uc <= 0x4bu) { // OP_PUSHBYTES_N
                size_t len = uc;
                if (s + 1 + len > srcLen) {
                    fprintf(stderr, "warn: missing data s=%zu; uc=%02x\n", s, uc);
                    break;
                }
                //dst[d] = uc; // skip wrapping
                ++s;
                if (pushIndex == countPushes) {
                    memcpy(&dst[d], &src[s], len);
                    d += len;
                    break;
                }
                ++countPushes;
                s += len;
            }
            else if (uc == 0x4cu || uc == 0x4du || uc == 0x4eu) { // OP_PUSHDATA_N
                size_t sz = 1 << (uc - 0x4cu);
                if (s + 1 + sz > srcLen) {
                    fprintf(stderr, "warn: missing data s=%zu; uc=%02x\n", s, uc);
                    break;
                }
                size_t len;
                len = (size_t) Bin2UIntLE(&src[s + 1], sz);
                if (s + 1 + sz + len > srcLen) {
                    fprintf(stderr, "warn: missing data sz=%zu; len=%zu s=%zu; uc=%02x\n", sz, len, s, uc);
                    break;
                }
                ++s;
                s += sz;
                if (pushIndex == countPushes) {
                    memcpy(&dst[d], &src[s], len);
                    d += len;
                    break;
                }
                ++countPushes;
                s += len;
            }
        }
        else {
            ++s;
        }
    }
    return d;
}
